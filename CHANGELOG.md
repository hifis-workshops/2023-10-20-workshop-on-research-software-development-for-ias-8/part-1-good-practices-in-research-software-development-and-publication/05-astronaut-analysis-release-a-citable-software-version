<!--
SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
SPDX-License-Identifier: CC-BY-4.0
-->


# Changelog

All notable changes will be documented in this file.

## [Unreleased]

## [1.0.2] - 2023-10-20

### Added

- Improve position of the citation hint

## [1.0.1] - 2023-10-20

### Added

- Add citation hint in the README

## [1.0.0] - 2023-10-20

### Added

- Initial series of plots

[unreleased]: https://codebase.helmholtz.cloud/hifis-workshops/2023-10-20-workshop-on-research-software-development-for-ias-8/part-1-good-practices-in-research-software-development-and-publication/05-astronaut-analysis-release-a-citable-software-version/compare/1.0.0...main
[1.0.0]: https://codebase.helmholtz.cloud/hifis-workshops/2023-10-20-workshop-on-research-software-development-for-ias-8/part-1-good-practices-in-research-software-development-and-publication/05-astronaut-analysis-release-a-citable-software-version/-/tags/1.0.0
